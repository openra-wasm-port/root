# OpenRA-Wasm-port

A port of [OpenRA](https://www.openra.net) to Web Assembly!

Official Repository: [https://github.com/OpenRA/OpenRA](https://github.com/OpenRA/OpenRA) ![Continuous Integration](https://github.com/OpenRA/OpenRA/workflows/Continuous%20Integration/badge.svg)

### Note:
This project as of now is relatively new, and so many of the required changes haven't been made yet.  
There will be a build of this game available on my website when it is at a stage where it can be properly ran in the browser.  

There will be a link here when it is available.

## Changes made
This is a relatively simple port to make. All we have to do is add Web Assembly support, which *should* be easy, but time will tell.

Some gameplay changes *do* have to be made, however. This is designed to be playable on Mobile devices, and would require a few tweaks to work.

> [!WARNING]
>The port may or may not work on actual OpenRA servers, but that I am unsure of.  
> It is not recommended to do so.

## License
Copyright (c) OpenRA Developers and Contributors
This file is part of OpenRA, which is free software. It is made
available to you under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version. For more
information, see [COPYING](https://github.com/OpenRA/OpenRA/blob/bleed/COPYING).
